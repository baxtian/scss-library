<?php

 namespace Tests;

 // Entorno de testeto
 use Tests\MonkeyTestCase;
 use Brain\Monkey;
 use Mockery;

 // Clases y dependencias a probar
 use ScssLibrary\Views\AdminNotifications;

 class ViewAdminNotificationsTest extends MonkeyTestCase
 {
 	protected function setUp(): void
 	{
 		parent::setUp();
 		Monkey\Functions\when('__')
			->returnArg(1);
 	}

	/**
 	 * @runInSeparateProcess
 	 * @preserveGlobalState disabled
 	 */
 	public function testWithoutMessage()
 	{
 		Monkey\Functions\when('get_option')
			 ->justReturn([]);

 		$sut = new AdminNotifications();

 		ob_start();
 		$sut::notifications();
 		$out = ob_get_contents();
 		ob_end_clean();

 		$this->assertTrue(empty($out));
 	}

 	/**
 	 * @runInSeparateProcess
 	 * @preserveGlobalState disabled
 	 */
 	public function testWithMessage()
 	{
 		$expected = <<<'EXPECTED'
		<div class="error"><p>The development mode from the <strong>SCSS-Library</strong> is active. Remember to <a href='/site/?dato=1&deactivate_scss_library_devmode=1'>deactivate it</a> in case this is a production environment.</p></div>
		EXPECTED;

 		$GLOBALS['_SERVER'] = [
 			'REQUEST_URI' => 'https://127.0.0.1/site/?dato=1',
 		];

 		Monkey\Functions\when('get_option')
			->justReturn(['dev_mode' => true]);

 		$sut = new AdminNotifications();

 		ob_start();
 		$sut::notifications();
 		$out = ob_get_contents();
 		ob_end_clean();

 		$expected = preg_replace('/\t+/', '', $expected);
 		$actual   = preg_replace('/\t+/', '', $out);

 		$this->assertEquals($expected, $actual);
 	}

 	/**
 	 * @runInSeparateProcess
 	 * @preserveGlobalState disabled
 	 */
 	public function testDeactivatingDevMode()
 	{
 		$GLOBALS['_GET'] = [
 			'deactivate_scss_library_devmode' => 1,
 		];

 		Monkey\Functions\when('get_option')
			->justReturn(['dev_mode' => true]);

 		Monkey\Functions\when('update_option')
			->justReturn(true);

 		$sut = new AdminNotifications();

 		ob_start();
 		$sut::notifications();
 		$out = ob_get_contents();
 		ob_end_clean();

 		$this->assertTrue(empty($out));
 	}
 }
