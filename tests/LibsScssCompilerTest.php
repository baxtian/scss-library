<?php

 namespace Tests;

 // Entorno de testeto
 use Tests\MonkeyTestCase;
 use Brain\Monkey;
 use Mockery;

 // Clases y dependencias a probar
 use ScssLibrary\Libs\ScssCompiler;
 use ScssLibrary\Libs\ScssLibrary;
 use Exception;
use Mockery\Matcher\Any;

 class LibsScssCompilerTest extends MonkeyTestCase
 {
 	protected function setUp(): void
 	{
 		parent::setUp();
 		Monkey\Functions\when('__')
			->returnArg(1);
 		Monkey\Functions\when('content_url')
			->justReturn('http://localhost/test/');

 		if (!defined('ABSPATH')) {
 			define('ABSPATH', __DIR__);
 		}
 	}

 	/**
 	 * @runInSeparateProcess
 	 * @preserveGlobalState disabled
 	 */
 	public function testCompile()
 	{
 		$scss            = Mockery::mock(ScssLibrary::class);
 		$scss->dev_mode  = false;
 		$scss->out_file  = 'out.css';
 		$scss->out_url   = 'out.css';
 		$scss->in_file   = __DIR__ . '/assets/style.scss';
 		$scss->variables = [];

 		$sut     = new ScssCompiler();
 		$results = $sut->compile($scss);
 		$this->assertEquals('.test{background:red}', $results->getCss());
 		$this->assertNull($results->getSourceMap());
 	}

 	/**
 	 * @runInSeparateProcess
 	 * @preserveGlobalState disabled
 	 */
 	public function testCompileWithMap()
 	{
 		$scss            = Mockery::mock(ScssLibrary::class);
 		$scss->dev_mode  = true;
 		$scss->out_file  = 'out.css';
 		$scss->out_url   = 'out.css';
 		$scss->in_file   = __DIR__ . '/assets/style.scss';
 		$scss->variables = [];

 		$sut     = new ScssCompiler();
 		$results = $sut->compile($scss);
 		$this->assertNotNull($results->getSourceMap());
 	}

 	/**
 	 * @runInSeparateProcess
 	 * @preserveGlobalState disabled
 	 */
 	public function testCompileWithError()
 	{
 		$scss            = Mockery::mock(ScssLibrary::class);
 		$scss->dev_mode  = true;
 		$scss->out_file  = 'out.css';
 		$scss->out_url   = 'out.css';
 		$scss->in_file   = __DIR__ . '/assets/file_not_exists.scss';
 		$scss->variables = [];

 		$this->expectException(Exception::class);

 		$sut = new ScssCompiler();
 		$sut->compile($scss);
 	}

 	public function testCompilationNotRequired()
 	{
 		Monkey\Functions\when('get_transient')
			->justReturn(sha1(serialize(['a' => 1])));
 		Monkey\Functions\when('filemtime')
			->justReturn(2);
 		Monkey\Functions\when('set_transient')
			->justReturn(true);
 		Monkey\Functions\when('file_exists')
			->justReturn(true);

 		$sut         = new ScssCompiler();
 		$filemtimes  = ['out' => 2];
 		$is_required = $sut->compilation_is_required('in', 'out', ['a' => 1], $filemtimes);
 		$this->assertFalse($is_required);
 	}

 	public function testCompilationRequiredEmpty()
 	{
 		$sut         = new ScssCompiler();
 		$filemtimes  = false;
 		$is_required = $sut->compilation_is_required('in', 'out', [], $filemtimes);
 		$this->assertEquals([], $filemtimes);
 		$this->assertTrue($is_required);
 	}

 	public function testCompilationRequiredNew()
 	{
 		Monkey\Functions\when('filemtime')
			->justReturn(2);

 		$sut         = new ScssCompiler();
 		$filemtimes  = ['out' => 1];
 		$is_required = $sut->compilation_is_required('in', 'out', [], $filemtimes);
 		$this->assertTrue($is_required);
 	}

 	public function testCompilationRequiredDifferentVars()
 	{
 		$sha = sha1('out');

 		Monkey\Functions\when('get_transient')
			->justReturn(['a' => 1]);
 		Monkey\Functions\when('filemtime')
			->justReturn(2);

 		Monkey\Functions\expect('set_transient')
			->once()
			->with('scsslib_variables_signature_' . $sha, Mockery::any());

 		$sut         = new ScssCompiler();
 		$filemtimes  = ['out' => 2];
 		$is_required = $sut->compilation_is_required('in', 'out', [], $filemtimes);
 		$this->assertTrue($is_required);
 	}

 	public function testCompilationRequiredStillNotCompiled()
 	{
 		Monkey\Functions\when('get_transient')
			->justReturn(sha1(serialize(['a' => 1])));
 		Monkey\Functions\when('filemtime')
			->justReturn(2);
 		Monkey\Functions\when('set_transient')
			->justReturn(true);

 		$sut         = new ScssCompiler();
 		$filemtimes  = ['out' => 2];
 		$is_required = $sut->compilation_is_required('in', 'out', ['a' => 1], $filemtimes);
 		$this->assertTrue($is_required);
 	}

 	public function testCorrectFmtimesArray()
 	{
 		Monkey\Functions\when('get_transient')
			->justReturn(sha1(serialize(['a' => 1])));

 		$sut         = new ScssCompiler();
 		$filemtimes  = 1;
 		$sut->compilation_is_required('in', 'out', ['a' => 1], $filemtimes);
 		$this->assertEquals([], $filemtimes);

		$filemtimes  = [];
 		$sut->compilation_is_required('in', 'out', ['a' => 1], $filemtimes);
 		$this->assertEquals([], $filemtimes);

		 $filemtimes  = ['a' => 1];
 		$sut->compilation_is_required('in', 'out', ['a' => 1], $filemtimes);
 		$this->assertEquals(['a' => 1], $filemtimes);
 	}
 }
