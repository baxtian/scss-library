<?php

 namespace Tests;

 // Entorno de testeto
 use Tests\MonkeyTestCase;
 use Brain\Monkey;
 use Mockery;

 // Clases y dependencias a probar
 use ScssLibrary\Views\Errors;
 use ScssLibrary\Libs\Errors as Data;

 class ViewErrorsTest extends MonkeyTestCase
 {
 	protected function setUp(): void
 	{
 		parent::setUp();
 		Monkey\Functions\when('_e')
			->echoArg(1);
 	}

 	/**
 	 * @runInSeparateProcess
 	 * @preserveGlobalState disabled
 	 */
 	public function testEmpty()
 	{
 		$sut = new Errors();

 		ob_start();
 		$sut::wp_footer();
 		$out = ob_get_contents();
 		ob_end_clean();

 		$this->assertTrue(empty($out));
 	}

	/**
 	 * @runInSeparateProcess
 	 * @preserveGlobalState disabled
 	 */
 	public function testWithErrors()
 	{
		$expected=<<<'EXPECTED'
		<style>
		#scsslib {
			position: fixed;
			top: 0;
			z-index: 99999;
			width: 100%;
			padding: 20px;
			overflow: auto;
			background: #f5f5f5;
			font-family: 'Source Code Pro', Menlo, Monaco, Consolas, monospace;
			font-size: 18px;
			color: #666;
			text-align: left;
			border-left: 5px solid #DD3D36;
		}
		body.admin-bar #scsslib {
			top: 32px;
		}
		#scsslib .scsslib-title {
			margin-bottom: 20px;
			font-size: 120%;
		}
		#scsslib .scsslib-error {
			margin: 10px 0;
		}
		#scsslib .scsslib-file {
			font-weight: bold;
			white-space: pre;
			white-space: pre-wrap;
			word-wrap: break-word;
		}
		#scsslib .scsslib-message {
			white-space: pre;
			white-space: pre-wrap;
			word-wrap: break-word;
		}
		</style>
		<div id="scsslib">
			<div class="scsslib-title">Sass Compiling Error</div>
			<div class="scsslib-error">
				<div class="scsslib-file">handle1 : file1</div>
				<div class="scsslib-message">message1</div>
			</div>
			<div class="scsslib-error">
				<div class="scsslib-file">handle2 : file2</div>
				<div class="scsslib-message">message2</div>
			</div>
		</div>
		EXPECTED;

 		$data = Data::get_instance();
 		$data->enqueue('handle1', 'file1', 'message1');
		$data->enqueue('handle2', 'file2', 'message2');

 		$sut = new Errors();

 		ob_start();
 		$sut::wp_footer();
 		$out = ob_get_contents();
 		ob_end_clean();

		$expected = trim(preg_replace('/\t+/', '', $expected));
		$actual = trim(preg_replace('/\t+/', '', $out));

 		$this->assertEquals($expected, $actual);
 	}
 }
