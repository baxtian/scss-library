<?php

 namespace Tests;

 // Entorno de testeto
 use Tests\MonkeyTestCase;
 use Brain\Monkey;
 use Mockery;

 // Clases y dependencias a probar
 use ScssLibrary\Settings\ScssLibrary;
 use Exception;

 class SettingsTest extends MonkeyTestCase
 {
 	protected function setUp(): void
 	{
 		parent::setUp();
 		Monkey\Functions\when('__')
			->returnArg(1);
 		Monkey\Functions\when('get_option')
			->returnArg(1);
 		Monkey\Functions\when('is_multisite')
			->justReturn(false);
 	}

	/**
	 * @runInSeparateProcess
	 * @preserveGlobalState disabled
	 */
 	public function testHooks()
 	{
 		ScssLibrary::get_instance();
 		$this->assertEquals(10, has_action('customize_register', 'ScssLibrary\Settings\ScssLibrary->options()'));
 	}

 	public function testDeclarOptions()
 	{
 		Monkey\Functions\when('load_plugin_textdomain')
			->justReturn(true);

 		// WP Customize
 		$wp_customize = Mockery::mock('alias:wp_customize');
 		$wp_customize->shouldReceive('add_section')
			->once()
			->andReturn(true);
 		$wp_customize->shouldReceive('add_setting')
			->once()
			->andReturn(true);
 		$wp_customize->shouldReceive('add_control')
			->once()
			->andReturn(true);

 		$sut = ScssLibrary::get_instance();;
 		$sut->options($wp_customize);
 	}
 }
