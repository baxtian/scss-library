<?php

 namespace Tests;

 // Entorno de testeto
 use Tests\MonkeyTestCase;
 use Brain\Monkey;
 use Mockery;

 // Clases y dependencias a probar
 use ScssLibrary\Init;
 use ScssLibrary\Libs\ScssLibrary;
 use ScssLibrary\Views\Errors;
 use ScssLibrary\Views\AdminNotifications;
 use ScssLibrary\Views\AdminBar;

 class InitTest extends MonkeyTestCase
 {
 	protected function setUp(): void
 	{
 		parent::setUp();
 		Monkey\Functions\when('__')
			->returnArg(1);
 		Monkey\Functions\when('get_option')
			->returnArg(1);
 		Monkey\Functions\when('is_multisite')
			->justReturn(false);
 	}

 	public function testHooks()
 	{
 		Init::get_instance();

 		$this->assertEquals(10, has_action('plugins_loaded', 'ScssLibrary\Init->plugin_setup()'));
 		$this->assertEquals(10, has_filter('style_loader_src', 'ScssLibrary\Init->style_loader_src()'));
 		$this->assertEquals(10, has_action('wp_footer', [Errors::class, 'wp_footer']));
 		$this->assertEquals(10, has_action('admin_notices', [AdminNotifications::class, 'notifications']));
 		$this->assertEquals(100, has_action('admin_bar_menu', [AdminBar::class, 'bar_menu']));
 	}

 	/**
 	 * @runInSeparateProcess
 	 * @preserveGlobalState disabled
 	 */
 	public function testPluginSetup()
 	{
 		// Monkey\Functions\when('load_plugin_textdomain')
		// 	->justReturn(true);
		Monkey\Functions\expect('load_plugin_textdomain')
			->once();

 		$sut = Init::get_instance();
 		$sut->plugin_setup();
 	}

 	/**
 	 * @runInSeparateProcess
 	 * @preserveGlobalState disabled
 	 */
 	public function testStyleLoader()
 	{
 		// Should call_set_directory once
 		$scsslibrary = Mockery::mock(ScssLibrary::class);
 		$scsslibrary->shouldReceive('style_loader_src')
		 ->andReturnUsing(
		 	function ($src, $handle) {
				return $src;
			}
		 );

 		$sut = Init::get_instance(['scss_library' => $scsslibrary]);
 		$this->assertEquals('src', $sut->style_loader_src('src', 'handle'));
 	}
 }
