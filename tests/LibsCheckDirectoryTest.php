<?php

 namespace Tests;

 // Entorno de testeto
 use Tests\MonkeyTestCase;
 use Brain\Monkey;
 use Mockery;

 // Clases y dependencias a probar
 use ScssLibrary\Libs\CheckDirectory;
 use ScssLibrary\Libs\Errors;
 use Exception;

use function PHPUnit\Framework\assertFalse;

 class LibsCheckDirectoryTest extends MonkeyTestCase
 {
 	protected function setUp(): void
 	{
 		parent::setUp();
 		Monkey\Functions\when('__')
	->returnArg(1);
 	}

 	/**
 	 * @runInSeparateProcess
 	 * @preserveGlobalState disabled
 	 */
 	public function testCheckOk()
 	{
 		Monkey\Functions\when('is_dir')
			->justReturn(true);
 		Monkey\Functions\when('wp_mkdir_p')
			->justReturn(true);

 		$sut = CheckDirectory::get_instance();
 		$this->assertTrue($sut->check(__DIR__));
 	}

 	/**
 	 * @runInSeparateProcess
 	 * @preserveGlobalState disabled
 	 */
 	public function testCanNotCreateDirectory()
 	{
 		Monkey\Functions\when('is_dir')
			->justReturn(false);
 		Monkey\Functions\when('wp_mkdir_p')
			->justReturn(false);

 		// Should call_set_directory once
 		$errors = Mockery::mock(Errors::class);
 		$errors->shouldReceive('enqueue')
			->once()
			->withArgs([
				'SCSS Library',
				'Cache directory.',
				'File Permissions Error, unable to create cache directory. Please make sure the Wordpress Uploads directory is writable.',
				Errors::DIRECTORY_CANNOT_CREATE,
			]);

 		$sut = CheckDirectory::get_instance(['errors' => $errors]);
 		$this->assertFalse($sut->check(__DIR__));
 	}

 	/**
 	 * @runInSeparateProcess
 	 * @preserveGlobalState disabled
 	 */
 	public function testCanNotWriteInDirectory()
 	{
 		Monkey\Functions\when('is_dir')
	->justReturn(true);
 		Monkey\Functions\when('is_writable')
	->justReturn(false);

 		// Should call_set_directory once
 		$errors = Mockery::mock(Errors::class);
 		$errors->shouldReceive('enqueue')
			->once()
			->withArgs([
				'SCSS Library',
				'Cache directory.',
				'File Permissions Error, permission denied. Please make ' . __DIR__ . ' writable.',
				Errors::DIRECTORY_PERMISSION_DENIED,
			]);

 		$sut = CheckDirectory::get_instance(['errors' => $errors]);
 		$this->assertFalse($sut->check(__DIR__));
 	}
 }
