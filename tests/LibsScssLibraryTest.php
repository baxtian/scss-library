<?php

 namespace Tests;

 // Entorno de testeto
 use Tests\MonkeyTestCase;
 use Brain\Monkey;
 use Mockery;

 // Clases y dependencias a probar
 use ScssLibrary\Libs\ScssLibrary;
 use ScssLibrary\Libs\ScssCompiler;
 use ScssLibrary\Libs\Errors;
 use ScssLibrary\Libs\CheckDirectory;
 use ScssPhp\ScssPhp\CompilationResult;
 use Exception;
use ScssLibrary\Libs\DevMode;

 class LibsScssLibraryTest extends MonkeyTestCase
 {
 	protected function setUp(): void
 	{
 		parent::setUp();
 		Monkey\Functions\when('__')
			->returnArg(1);
 		Monkey\Functions\when('is_multisite')
			->justReturn(false);
 		Monkey\Functions\when('get_current_blog_id')
			->justReturn(3);
 		Monkey\Functions\when('file_exists')
			->justReturn(true);
 		Monkey\Functions\when('get_template_directory_uri')
			->justReturn('');
 		Monkey\Functions\when('get_stylesheet_directory_uri')
			->justReturn('');
 		Monkey\Functions\when('apply_filters')
			->returnArg(2);
 		Monkey\Functions\when('get_transient')
			->justReturn([]);
 		Monkey\Functions\when('set_transient')
			->justReturn(true);
 		Monkey\Functions\when('file_put_contents')
			->justReturn(true);
 		Monkey\Functions\when('filemtime')
			->justReturn(true);

 		Monkey\Functions\when('site_url')
			->justReturn('http://localhost');

 		if (!defined('WP_CONTENT_URL')) {
 			define('WP_CONTENT_URL', 'http://localhost/wp-content');
 			define('WP_CONTENT_DIR', '/srv/www/wp-content');
 		}

 		$GLOBALS['_SERVER'] = [
 			'DOCUMENT_ROOT' => __DIR__,
 		];
 	}

 	/**
 	 * @runInSeparateProcess
 	 * @preserveGlobalState disabled
 	 */
 	public function testCompile()
 	{
 		// DevMode
 		$dev_mode = Mockery::mock(DevMode::class);
 		$dev_mode->shouldReceive('is_active')
		   ->andReturn(false);
 		$dev_mode->shouldReceive('required_action')
		   ->andReturn(false);

 		// CheckDirectory
 		$check_directory = Mockery::mock('alias:' . CheckDirectory::class);
 		$check_directory->shouldReceive('check')
		   ->andReturn(true);

 		// Results
 		$results = Mockery::mock(CompilationResult::class);
 		$results->shouldReceive('getCss')
			 ->andReturn('out.css');
 		$results->shouldReceive('getSourceMap')
			 ->andReturn('out.css.map');

 		// Compiler
 		$compiler = Mockery::mock('alias:' . ScssCompiler::class);
 		$compiler->shouldReceive('compile')
			 ->andReturn($results);
 		$compiler->shouldReceive('compilation_is_required')
			 ->andReturn(true);

 		$sut = ScssLibrary::get_instance([
 			'compiler'        => $compiler,
 			'check_directory' => $check_directory,
 			'dev_mode'        => $dev_mode,
 		]);

 		$file = '/wp-content/plugins/test/test.scss';
 		$sha  = sha1($file) . '.css';

 		$src = $sut->style_loader_src('http://localhost' . $file, 'handle');
 		$this->assertEquals('http://localhost/wp-content/build/scss_library/' . $sha, $src);
 		$this->assertTrue($sut->compiled);
 	}

 	/**
 	 * @runInSeparateProcess
 	 * @preserveGlobalState disabled
 	 */
 	public function testCompileMultisite()
 	{
 		Monkey\Functions\when('is_multisite')
			->justReturn(true);

 		// DevMode
 		$dev_mode = Mockery::mock(DevMode::class);
 		$dev_mode->shouldReceive('is_active')
			->andReturn(false);
 		$dev_mode->shouldReceive('required_action')
			->andReturn(false);

 		// CheckDirectory
 		$check_directory = Mockery::mock('alias:' . CheckDirectory::class);
 		$check_directory->shouldReceive('check')
			->andReturn(true);

 		// Results
 		$results = Mockery::mock(CompilationResult::class);
 		$results->shouldReceive('getCss')
			  ->andReturn('out.css');
 		$results->shouldReceive('getSourceMap')
			  ->andReturn('out.css.map');

 		// Compiler
 		$compiler = Mockery::mock('alias:' . ScssCompiler::class);
 		$compiler->shouldReceive('compile')
			  ->andReturn($results);
 		$compiler->shouldReceive('compilation_is_required')
			  ->andReturn(true);

 		$sut = ScssLibrary::get_instance([
 			'compiler'        => $compiler,
 			'check_directory' => $check_directory,
 			'dev_mode'        => $dev_mode,
 		]);

 		Monkey\Functions\when('get_blog_details')
			->justReturn((object) ['path' => '/sitio/3/']);

 		define('PATH_CURRENT_SITE', '/sitio/');

 		$file = '/wp-content/plugins/test/test.scss';
 		$sha  = sha1($file) . '.css';

 		$src = $sut->style_loader_src('http://localhost' . $file, 'handle');
 		$this->assertEquals('http://localhost/wp-content/build/scss_library/3/' . $sha, $src);
 		$this->assertTrue($sut->compiled);
 	}

 	/**
 	 * @runInSeparateProcess
 	 * @preserveGlobalState disabled
 	 */
 	public function testNotCompile()
 	{
 		// DevMode
 		$dev_mode = Mockery::mock(DevMode::class);
 		$dev_mode->shouldReceive('is_active')
			->andReturn(false);
 		$dev_mode->shouldReceive('required_action')
			->andReturn(false);

 		// CheckDirectory
 		$check_directory = Mockery::mock('alias:' . CheckDirectory::class);
 		$check_directory->shouldReceive('check')
			->andReturn(true);

 		// Compiler
 		$compiler = Mockery::mock('alias:' . ScssCompiler::class);
 		$compiler->shouldReceive('compilation_is_required')
			  ->andReturn(false);

 		$sut = ScssLibrary::get_instance([
 			'compiler'        => $compiler,
 			'check_directory' => $check_directory,
 			'dev_mode'        => $dev_mode,
 		]);

 		$file = '/wp-content/plugins/test/test.scss';
 		$sha  = sha1($file) . '.css';

 		$src = $sut->style_loader_src('http://localhost' . $file, 'handle');
 		$this->assertEquals('http://localhost/wp-content/build/scss_library/' . $sha, $src);
 		$this->assertFalse($sut->compiled);
 	}

 	/**
 	 * @runInSeparateProcess
 	 * @preserveGlobalState disabled
 	 */
 	public function testDirectoryFailure()
 	{
 		Monkey\Functions\expect('delete_transient')
			->once();

 		// DevMode
 		$dev_mode = Mockery::mock(DevMode::class);
 		$dev_mode->shouldReceive('is_active')
			 ->andReturn(false);

 		// CheckDirectory
 		$check_directory = Mockery::mock('alias:' . CheckDirectory::class);
 		$check_directory->shouldReceive('check')
			 ->andReturn(false);

 		$sut = ScssLibrary::get_instance([
 			'check_directory' => $check_directory,
 			'dev_mode'        => $dev_mode,
 		]);

 		$file = 'http://localhost/wp-content/plugins/test/test.scss';
 		$sha  = sha1($file) . '.css';

 		$src = $sut->style_loader_src($file, 'handle');
 		$this->assertEquals($file, $src);
 		$this->assertFalse($sut->compiled);
 	}

 	/**
 	 * @runInSeparateProcess
 	 * @preserveGlobalState disabled
 	 */
 	public function testNotScss()
 	{
 		// DevMode
 		$dev_mode = Mockery::mock(DevMode::class);
 		$dev_mode->shouldReceive('is_active')
			  ->andReturn(false);

 		// CheckDirectory
 		$check_directory = Mockery::mock('alias:' . CheckDirectory::class);
 		$check_directory->shouldReceive('check')
			  ->andReturn(true);

 		$sut = ScssLibrary::get_instance([
 			'check_directory' => $check_directory,
 			'dev_mode'        => $dev_mode,
 		]);

 		$file = 'http://localhost/wp-content/plugins/test/test.css';

 		$src = $sut->style_loader_src($file, 'handle');
 		$this->assertEquals($file, $src);
 		$this->assertFalse($sut->compiled);
 	}

 	/**
 	 * @runInSeparateProcess
 	 * @preserveGlobalState disabled
 	 */
 	public function testSourceNotFound()
 	{
 		Monkey\Functions\when('file_exists')
			->justReturn(false);

 		// Errors
 		$errors = Errors::get_instance();

 		// DevMode
 		$dev_mode = Mockery::mock(DevMode::class);
 		$dev_mode->shouldReceive('is_active')
			   ->andReturn(false);

 		// CheckDirectory
 		$check_directory = Mockery::mock('alias:' . CheckDirectory::class);
 		$check_directory->shouldReceive('check')
			   ->andReturn(true);

 		$sut = ScssLibrary::get_instance([
 			'check_directory' => $check_directory,
 			'dev_mode'        => $dev_mode,
 		]);

 		$file = 'http://localhost/wp-content/plugins/test/test.scss';

 		$src = $sut->style_loader_src($file, 'handle');
 		$this->assertEquals($file, $src);
 		$this->assertFalse($sut->compiled);

 		// An errors has to be created
 		$list_errors = $errors->errors;
 		$this->assertEquals(1, count($list_errors));
 		$this->assertEquals(Errors::SOURCE_NOT_FOUND, $list_errors[0]['code']);
 	}

 	/**
 	 * @runInSeparateProcess
 	 * @preserveGlobalState disabled
 	 */
 	public function testIgnoreExternal()
 	{
 		// DevMode
 		$dev_mode = Mockery::mock(DevMode::class);
 		$dev_mode->shouldReceive('is_active')
			  ->andReturn(false);

 		// CheckDirectory
 		$check_directory = Mockery::mock('alias:' . CheckDirectory::class);
 		$check_directory->shouldReceive('check')
			  ->andReturn(true);

 		$sut = ScssLibrary::get_instance([
 			'check_directory' => $check_directory,
 			'dev_mode'        => $dev_mode,
 		]);

 		$file = 'http://test.com/wp-content/plugins/test/test.scss';

 		$src = $sut->style_loader_src($file, 'handle');
 		$this->assertEquals($file, $src);
 		$this->assertFalse($sut->compiled);
 	}

 	/**
 	 * @runInSeparateProcess
 	 * @preserveGlobalState disabled
 	 */
 	public function testDevModeEnabled()
 	{
 		// DevMode
 		$dev_mode = Mockery::mock(DevMode::class);
 		$dev_mode->shouldReceive('is_active')
			->andReturn(true);

 		$sut = ScssLibrary::get_instance(['dev_mode' => $dev_mode]);
 		$this->assertTrue($sut->dev_mode);
 	}

 	/**
 	 * @runInSeparateProcess
 	 * @preserveGlobalState disabled
 	 */
 	public function testDevModeDisabled()
 	{
 		// DevMode
 		$dev_mode = Mockery::mock(DevMode::class);
 		$dev_mode->shouldReceive('is_active')
			->andReturn(false);

 		$sut = ScssLibrary::get_instance(['dev_mode' => $dev_mode]);
 		$this->assertFalse($sut->dev_mode);
 	}

 	/**
 	 * @runInSeparateProcess
 	 * @preserveGlobalState disabled
 	 */
 	public function testDirectories()
 	{
 		// DevMode
 		$dev_mode = Mockery::mock(DevMode::class);
 		$dev_mode->shouldReceive('is_active')
			 ->andReturn(false);

 		$sut = ScssLibrary::get_instance(['dev_mode' => $dev_mode]);
 		$this->assertEquals('/srv/www/wp-content/build/scss_library/', $sut->build_dir);
 		$this->assertEquals('http://localhost/wp-content/build/scss_library/', $sut->build_url);
 	}

 	/**
 	 * @runInSeparateProcess
 	 * @preserveGlobalState disabled
 	 */
 	public function testDirectoriesMultisite()
 	{
 		Monkey\Functions\when('is_multisite')
			->justReturn(true);

 		// DevMode
 		$dev_mode = Mockery::mock(DevMode::class);
 		$dev_mode->shouldReceive('is_active')
			 ->andReturn(false);

 		$sut = ScssLibrary::get_instance(['dev_mode' => $dev_mode]);
 		$this->assertEquals('/srv/www/wp-content/build/scss_library/3/', $sut->build_dir);
 		$this->assertEquals('http://localhost/wp-content/build/scss_library/3/', $sut->build_url);
 	}

 	/**
 	 * @runInSeparateProcess
 	 * @preserveGlobalState disabled
 	 */
 	public function testCompilerException()
 	{
 		// Errors
 		$errors = Errors::get_instance();

 		// DevMode
 		$dev_mode = Mockery::mock(DevMode::class);
 		$dev_mode->shouldReceive('is_active')
		   ->andReturn(true);

 		// CheckDirectory
 		$check_directory = Mockery::mock('alias:' . CheckDirectory::class);
 		$check_directory->shouldReceive('check')
		   ->andReturn(true);

 		// Compiler
 		$compiler = Mockery::mock('alias:' . ScssCompiler::class);
 		$compiler->shouldReceive('compile')
			 ->andThrow(new Exception('error', 1000));

 		$sut = ScssLibrary::get_instance([
 			'compiler'        => $compiler,
 			'check_directory' => $check_directory,
 			'dev_mode'        => $dev_mode,
 		]);

 		$file = 'http://localhost/wp-content/plugins/test/test.scss';

 		$sut->style_loader_src($file, 'handle');
 		$this->assertFalse($sut->compiled);

 		// An errors has to be created
 		$list_errors = $errors->errors;
 		$this->assertEquals(1, count($list_errors));
 		$this->assertEquals(1000, $list_errors[0]['code']);
 	}

 	/**
 	 * @runInSeparateProcess
 	 * @preserveGlobalState disabled
 	 */
 	public function testRemoveMap()
 	{
 		// DevMode
 		$dev_mode = Mockery::mock(DevMode::class);
 		$dev_mode->shouldReceive('is_active')
			->andReturn(false);
 		$dev_mode->shouldReceive('required_action')
			->andReturn(false);

 		// CheckDirectory
 		$check_directory = Mockery::mock('alias:' . CheckDirectory::class);
 		$check_directory->shouldReceive('check')
			->andReturn(true);

 		// Results
 		$results = Mockery::mock(CompilationResult::class);
 		$results->shouldReceive('getCss')
			->andReturn('out.css');
 		$results->shouldReceive('getSourceMap')
			->andReturn(null);

 		// Compiler
 		$compiler = Mockery::mock('alias:' . ScssCompiler::class);
 		$compiler->shouldReceive('compile')
			  ->andReturn($results);
 		$compiler->shouldReceive('compilation_is_required')
			  ->andReturn(true);

 		$sut = ScssLibrary::get_instance([
 			'compiler'        => $compiler,
 			'check_directory' => $check_directory,
 			'dev_mode'        => $dev_mode,
 		]);

 		$file = '/wp-content/plugins/test/test.scss';
 		$sha  = sha1($file) . '.css';

 		Monkey\Functions\expect('unlink')
			->once()
			->with(WP_CONTENT_DIR . '/build/scss_library/' . $sha . '.map')
			->andReturn(true);

 		$src = $sut->style_loader_src('http://localhost' . $file, 'handle');
 		$this->assertEquals('http://localhost/wp-content/build/scss_library/' . $sha, $src);
 	}
 }
