<?php

 namespace Tests;

 // Entorno de testeto
 use Tests\MonkeyTestCase;
 use Brain\Monkey;
 use Mockery;

 // Clases y dependencias a probar
 use ScssLibrary\Libs\DevMode;

 class LibsDevModeTest extends MonkeyTestCase
 {
 	protected function setUp(): void
 	{
 		parent::setUp();
 		Monkey\Functions\when('__')
			->returnArg(1);
 		Monkey\Functions\when('update_option')
			->justReturn(true);
 	}

 	/**
 	 * @runInSeparateProcess
 	 * @preserveGlobalState disabled
 	 */
 	public function testCheckInDevMode()
 	{
 		Monkey\Functions\when('get_option')
			->justReturn(['dev_mode' => true]);

 		$sut = DevMode::get_instance();
 		$this->assertTrue($sut->is_active());
 	}

 	/**
 	 * @runInSeparateProcess
 	 * @preserveGlobalState disabled
 	 */
 	public function testCheckDebuging()
 	{
 		Monkey\Functions\when('get_option')
			 ->justReturn(['dev_mode' => false]);

 		define('WP_DEBUG', true);

 		$sut = DevMode::get_instance();
 		$this->assertTrue($sut->is_active());
 	}

 	/**
 	 * @runInSeparateProcess
 	 * @preserveGlobalState disabled
 	 */
 	public function testCheckActivate()
 	{
 		Monkey\Functions\when('get_option')
			  ->justReturn([]);

 		$GLOBALS['_GET'] = [
 			'activate_scss_library_devmode' => 1,
 		];

 		$sut = DevMode::get_instance();
 		$this->assertTrue($sut->is_active());
 	}

 	/**
 	 * @runInSeparateProcess
 	 * @preserveGlobalState disabled
 	 */
 	public function testCheckDeactivate()
 	{
 		Monkey\Functions\when('get_option')
			->justReturn(['dev_mode' => true]);

 		$sut = DevMode::get_instance();
 		$this->assertTrue($sut->is_active());

 		$GLOBALS['_GET'] = [
 			'deactivate_scss_library_devmode' => 1,
 		];
 		$this->assertFalse($sut->is_active());
 	}

 	/**
 	 * @runInSeparateProcess
 	 * @preserveGlobalState disabled
 	 */
 	public function testActionActivate()
 	{
 		Monkey\Functions\when('get_option')
			 ->justReturn(['dev_mode' => false]);

 		$sut     = DevMode::get_instance();
 		$options = $sut->activate();
 		$this->assertEquals(true, $options['dev_mode']);
 	}

 	/**
 	 * @runInSeparateProcess
 	 * @preserveGlobalState disabled
 	 */
 	public function testActionDeactivate()
 	{
 		Monkey\Functions\when('get_option')
			 ->justReturn(['dev_mode' => true]);

 		$sut     = DevMode::get_instance();
 		$options = $sut->deactivate();
 		$this->assertEquals(false, $options['dev_mode']);
 	}

 	public function testRequiredAction()
 	{
 		$sut = DevMode::get_instance();
 		$this->assertFalse($sut->required_action());

 		$GLOBALS['_GET'] = [
 			'recompile_scss_files' => 1,
 		];
 		$this->assertTrue($sut->required_action());

 		$GLOBALS['_GET'] = [
 			'activate_scss_library_devmode' => 1,
 		];
 		$this->assertTrue($sut->required_action());

 		$GLOBALS['_GET'] = [
 			'deactivate_scss_library_devmode' => 1,
 		];
 		$this->assertTrue($sut->required_action());

 		$GLOBALS['_GET'] = [];
 		$this->assertFalse($sut->required_action());
 	}
 }
