<?php

 namespace Tests;

 // Entorno de testeto
 use Tests\MonkeyTestCase;
 use Brain\Monkey;
 use Mockery;

 // Clases y dependencias a probar
 use ScssLibrary\Libs\Errors;

 class LibsErrorsTest extends MonkeyTestCase
 {
	public function testGetNonexistsneVariable() {
		$sut = Errors::get_instance();

		// Empty list of errors
		$non_existent = $sut->non_existent;
 		$this->assertEquals(null, $non_existent);
	}

	/**
 	 * @runInSeparateProcess
 	 * @preserveGlobalState disabled
 	 */
 	public function testError()
 	{
 		$sut = Errors::get_instance();

		// Empty list of errors
		$errors = $sut->errors;
 		$this->assertEquals([], $errors);

		// Add error
		$sut->enqueue('handle1', 'file1', 'message1');
		$errors = $sut->errors;
		$this->assertEquals(1, count($errors));

		// Add error
		$sut->enqueue('handle2', 'file2', 'message2');
		$errors = $sut->errors;
		$this->assertEquals(2, count($errors));
		
		// Check list of errors
		$sut->enqueue('handle3', 'file3', 'message3');
		$errors = $sut->errors;
		$this->assertEquals('handle1', $errors[0]['handle']);
		$this->assertEquals('file2', $errors[1]['file']);
		$this->assertEquals('message3', $errors[2]['message']);
 	}
 }
