<?php

 namespace Tests;

 // Entorno de testeto
 use Tests\MonkeyTestCase;
 use Brain\Monkey;
 use Mockery;

 // Clases y dependencias a probar
 use ScssLibrary\Views\AdminBar;

 class ViewAdminBarTest extends MonkeyTestCase
 {
 	protected function setUp(): void
 	{
 		parent::setUp();
 		Monkey\Functions\when('__')
			->returnArg(1);
 	}

 	public function testUserWithoutPermissions()
 	{
 		Monkey\Functions\when('current_user_can')
			 ->justReturn(false);

 		$admin_bar = Mockery::mock('alias:admin_bar');
 		$admin_bar->shouldReceive('add_menu')
			->never();

 		$sut = new AdminBar();

 		$sut::bar_menu($admin_bar);
 	}

 	public function testFrontEndBar()
 	{
 		Monkey\Functions\when('current_user_can')
			->justReturn(true);
 		Monkey\Functions\when('get_option')
			->justReturn(['dev_mode' => true]);
 		Monkey\Functions\when('update_option')
			->justReturn(true);
 		Monkey\Functions\when('is_admin')
			->justReturn(false);

 		$GLOBALS['_SERVER'] = [
 			'REQUEST_URI' => 'https://127.0.0.1/site/?dato=1',
 		];

 		$admin_bar = Mockery::mock('alias:admin_bar');
 		$admin_bar->shouldReceive('add_menu')
			->once()
			->with([
				'id'    => 'scss-library',
				'title' => 'SCSS Library',
				'href'  => '#',
				'meta'  => [
					'class' => 'sl-alert',
					'html'  => '<style>#wpadminbar .menupop.sl-alert > a.ab-item { color: white; background: #9c3e3d; }</style>',
				],
			]);

 		$admin_bar->shouldReceive('add_menu')
			->once()
			->with([
				'id'     => 'clear-scss',
				'parent' => 'scss-library',
				'title'  => 'Recompile SCSS files',
				'href'   => '/site/?recompile_scss_files=1',
			]);

 		$admin_bar->shouldReceive('add_menu')
			->once()
			->with([
				'id'     => 'deactivate-scss-devmode',
				'parent' => 'scss-library',
				'title'  => 'Deactivate development mode',
				'href'   => '/site/?deactivate_scss_library_devmode=1',
				'meta'   => [
					'class' => 'sl-active',
					'html'  => '<style>#wpadminbar .ab-submenu .sl-active > a.ab-item { color: white; background: #9c3e3d; }</style>',
				],
			]);

 		$sut = new AdminBar();

 		$sut::bar_menu($admin_bar);
 	}

 	public function testDashboard()
 	{
 		Monkey\Functions\when('current_user_can')
			->justReturn(true);
 		Monkey\Functions\when('get_option')
			->justReturn(['dev_mode' => true]);
 		Monkey\Functions\when('update_option')
			->justReturn(true);
 		Monkey\Functions\when('is_admin')
			->justReturn(true);

 		$GLOBALS['_SERVER'] = [
 			'REQUEST_URI' => 'https://127.0.0.1/site/?dato=1',
 		];

 		$admin_bar = Mockery::mock('alias:admin_bar');
 		$admin_bar->shouldReceive('add_menu')
			->times(2);

 		$sut = new AdminBar();

 		$sut::bar_menu($admin_bar);
 	}

 	public function testActivateDevMode()
 	{
 		Monkey\Functions\when('current_user_can')
			->justReturn(true);
 		Monkey\Functions\when('get_option')
			->justReturn(['dev_mode' => true]);
 		Monkey\Functions\when('update_option')
			->justReturn(true);
 		Monkey\Functions\when('is_admin')
			->justReturn(false);

 		$GLOBALS['_SERVER'] = [
 			'REQUEST_URI' => 'https://127.0.0.1/site/?dato=1',
 		];

 		$GLOBALS['_GET'] = [
 			'activate_scss_library_devmode' => 1,
 		];

 		$admin_bar = Mockery::mock('alias:admin_bar');
 		$admin_bar->shouldReceive('add_menu')
			->once()
			->with([
				'id'    => 'scss-library',
				'title' => 'SCSS Library',
				'href'  => '#',
				'meta'  => [
					'class' => 'sl-alert',
					'html'  => '<style>#wpadminbar .menupop.sl-alert > a.ab-item { color: white; background: #9c3e3d; }</style>',
				],
			]);

 		$admin_bar->shouldReceive('add_menu')
			->once()
			->with([
				'id'     => 'clear-scss',
				'parent' => 'scss-library',
				'title'  => 'Recompile SCSS files',
				'href'   => '/site/?recompile_scss_files=1',
			]);

 		$admin_bar->shouldReceive('add_menu')
			->once()
			->with([
				'id'     => 'deactivate-scss-devmode',
				'parent' => 'scss-library',
				'title'  => 'Deactivate development mode',
				'href'   => '/site/?deactivate_scss_library_devmode=1',
				'meta'   => [
					'class' => 'sl-active',
					'html'  => '<style>#wpadminbar .ab-submenu .sl-active > a.ab-item { color: white; background: #9c3e3d; }</style>',
				],
			]);

 		$sut = new AdminBar();

 		$sut::bar_menu($admin_bar);
 	}

 	public function testDeactivateDevMode()
 	{
 		Monkey\Functions\when('current_user_can')
			->justReturn(true);
 		Monkey\Functions\when('get_option')
			->justReturn(['dev_mode' => true]);
 		Monkey\Functions\when('update_option')
			->justReturn(true);
 		Monkey\Functions\when('is_admin')
			->justReturn(true);

 		$GLOBALS['_SERVER'] = [
 			'REQUEST_URI' => 'https://127.0.0.1/site/?dato=1',
 		];

 		$GLOBALS['_GET'] = [
 			'deactivate_scss_library_devmode' => 1,
 		];

 		$admin_bar = Mockery::mock('alias:admin_bar');
 		$admin_bar->shouldReceive('add_menu')
			->once()
			->with([
				'id'    => 'scss-library',
				'title' => 'SCSS Library',
				'href'  => '#',
				'meta'  => [
					'class' => '',
					'html'  => '<style>#wpadminbar .menupop.sl-alert > a.ab-item { color: white; background: #9c3e3d; }</style>',
				],
			]);

 		$admin_bar->shouldReceive('add_menu')
			->once()
			->with([
				'id'     => 'activate-scss-devmode',
				'parent' => 'scss-library',
				'title'  => 'Activate development mode',
				'href'   => '/site/?activate_scss_library_devmode=1',
			]);

 		$sut = new AdminBar();

 		$sut::bar_menu($admin_bar);
 	}
 }
